<div id="footer" class="footer">
	<div class="wrapper">

		<span class="stay"><b>STAY IN TOUCH</b></span>

		<div id="social">
			<a href="" class="social"><img src="images/facebook.png" class="dim"></a>
			<a href="" class="social"><img src="images/twitter.png" class="dim"></a>
			<a href="" class="social"><img src="images/flikr.png" class="dim"></a>
			<a href="" class="social"><img src="images/vimeo.png" class="dim"></a>
		</div>

		<p class="foot">&copy; <?php echo date('Y'); ?> &nbsp; Yeti Guide Hiking</p>
	</div>

</div>


</div>
</div>
</body>
</html>