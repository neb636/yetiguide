<?php
include( 'header.php' );
$id = intval( $_GET['id'] );
$res = mysqli_query( $connection, "SELECT * FROM trail WHERE trail.id = $id" );
$row = mysqli_fetch_assoc( $res );
$latitude = $row['LATITUDE'];
$longitude = $row['LONGITUDE'];
$image = $row['TRAIL_PICS'];
$later = $row['TRAIL_NAME']; ?>

<div class="page_head_block">
	<div class="wrapper">
		<div id="pagename">
			<h1><?php echo ucwords( $row['TRAIL_NAME'] ); ?></h1>
		</div>
	</div>
</div>

<div id="user_wrap">
	<div class="wrapper">

		<div id="scroller">
		  <div class="nav">
		    <a class="prev">&laquo;</a>
		    <a class="next">&raquo;</a>
		  </div>
		  <a class="item" href="#"><img src="images/bike.jpg" /></a>
		  <a class="item" href="#"><img src="images/cliff.jpg" /></a>
		  <a class="item" href="#"><img src="images/break.jpg" /></a>
		  <a class="item" href="#"><img src="images/cutler.jpg" /></a>
		  <a class="item" href="#"><img src="images/moat.jpg" /></a>
		  <a class="item" href="#"><img src="images/perg.jpg" /></a>
		</div>
<div id="view_left">

<script>
/* Create an array to hold the different image positions */
var itemPositions = [];
var numberOfItems = $('#scroller .item').length;

/* Assign each array element a CSS class based on its initial position */
function assignPositions() {
    for (var i = 0; i < numberOfItems; i++) {
        if (i === 0) {
            itemPositions[i] = 'left-hidden';
        } else if (i === 1) {
            itemPositions[i] = 'left';
        } else if (i === 2) {
            itemPositions[i] = 'middle';
        } else if (i === 3) {
            itemPositions[i] = 'right';
        } else {
            itemPositions[i] = 'right-hidden';
        }
    }
    /* Add each class to the corresponding element */
    $('#scroller .item').each(function(index) {
        $(this).addClass(itemPositions[index]);
    });
}

/* To scroll, we shift the array values by one place and reapply the classes to the images */
function scroll(direction) {
    if (direction === 'prev') {
        itemPositions.push(itemPositions.shift());
    } else if (direction === 'next') {
        itemPositions.unshift(itemPositions.pop());
    }
    $('#scroller .item').removeClass('left-hidden left middle right right-hidden').each(function(index) {
        $(this).addClass(itemPositions[index]);
    });
}

/* Do all this when the DOMs ready */
$(document).ready(function() {

    assignPositions();
    var autoScroll = window.setInterval("scroll('next')", 4000);

    /* Hover behaviours */
    $('#scroller').hover(function() {
        window.clearInterval(autoScroll);
        $('.nav').stop(true, true).fadeIn(200);
    }, function() {
        autoScroll = window.setInterval("scroll('next')", 4000);
        $('.nav').stop(true, true).fadeOut(200);
    });

    /* Click behaviours */
    $('.prev').click(function() {
        scroll('prev');
    });
    $('.next').click(function() {
        scroll('next');
    });

});
</script>
<?php
	//echo "<div id='imge'><img src='images/$image' class='trailimage'></div>";
	echo $row["DESCRIPTION"] . "<br /><br />";

	if (isset($row["DIFFICULTY"]))
		echo "<p class='urs'>Difficulty: " . ucwords($row["DIFFICULTY"]) . "</p>";
	if (isset($row["RAITING"]))
		echo "<p>Raiting: " . ($row["RAITING"]) . "</p>";

	echo "<p>State: " . ucwords($row["STATE"]) . "</p>";

	if (isset($row["LENGTH"]))
		echo "<p>Length: " . ($row["LENGTH"]) . "&nbsp;Miles</p>";
	if (isset($row["DURATION"]))
		echo "<p>Duration: " . ($row["DURATION"]) . "&nbsp;Hours</p>";
	if (isset($row["GAIN"]) || !empty($row["GAIN"]))
			echo "<p>Elevation Gain: " . ($row["GAIN"]) . "&nbsp;Feet</p>";

	echo "<p>User who added trail: " . ($row["USERNAME"]) . "</p>";

	if (isset($row["DATE_ADD"]))
		echo "<p>Date Added" . ($row["DATE_ADD"]) . "</p>";

	if ($row["USERNAME"] == $_SESSION["USERNAME"] && $row["USERNAME"] != "") {
		echo "<a href='edit_trail.php?id=$id'>edit</a> &nbsp;";
	}

	$trl = $row["TRAIL_NAME"];

	?>

	<script src="http://maps.googleapis.com/maps/api/js?v=3.12&key=AIzaSyDZLirHNm_kxibIzyajeKaqv-Bwu1bvwSQ&sensor=false"></script>
	<script>
	// Enable the visual refresh
	google.maps.visualRefresh = true;

	var lat = <?php echo $latitude; ?>;
	var lon = <?php echo $longitude; ?>;
	var myLatlng = new google.maps.LatLng(lat, lon);
	var map;

	function initialize() {
	  var mapOptions = {
	    zoom: 14,
	    center: new google.maps.LatLng(lat, lon),
	    mapTypeId: google.maps.MapTypeId.TERRAIN
	  };
	  map = new google.maps.Map(document.getElementById('map-canvas'),
	      mapOptions);
	  var marker = new google.maps.Marker({
	      position: myLatlng,
	      map: map,
	      title: '<?php echo $trl; ?>'
	  });
	}

	google.maps.event.addDomListener(window, 'load', initialize);
	</script>

	<div id="map-canvas" class="map"></div>
</div>

<div id="view_right">
	<div class="weather_widget">
		<?php forecast($latitude, $longitude); ?>
	   <a class="linked" href="http://forecast.io/#/f/<?php echo $latitude . ',' . $longitude; ?>">View in more detail</a>
	</div>

	<div id="com_block">
		<div class="chatter">

		  <div class="chatter_pre_signup">
		  <input type="text" name="chatter_name" placeholder="Your name" class="chatter_field chatter_name" />
		  <input type="text" name="chatter_email" placeholder="Your email address" class="chatter_field chatter_email" />
		  <input type="submit" name="chatter_create_user" value="Start Chatting" class="chatter_btn chatter_create_user" />
		  </div>

		  <div class="chatter_post_signup">
		    <div class="chatter_convo">

		      <span class="feed_box">
		        <a href="" class="chatter_avatar"><img src="https://si0.twimg.com/profile_images/3003485948/349f8da7e56d9abc30f5f405a6ab76c4_bigger.jpeg" /></a>
		        <strong class="chatter_name">Luke Peters</strong>The weather is great today. Loving this trail. I suggest coming for a hike before the sun sets.</span>

		      <span class="feed_box">
		        <a href="" class="chatter_avatar"><img src="https://si0.twimg.com/profile_images/3003485948/349f8da7e56d9abc30f5f405a6ab76c4_bigger.jpeg" /></a>
		        <strong class="chatter_name">Luke Peters</strong>Super fun trails!</span>

		      <span class="feed_box">
		        <a href="" class="chatter_avatar"><img src="https://si0.twimg.com/profile_images/3003485948/349f8da7e56d9abc30f5f405a6ab76c4_bigger.jpeg" /></a>
		        <strong class="chatter_name">Luke Peters</strong>Oh alright, Hello then Jack, you pirate, you.</span>

		      <span class="feed_box">
		        <a href="" class="chatter_avatar"><img src="https://si0.twimg.com/profile_images/3003485948/349f8da7e56d9abc30f5f405a6ab76c4_bigger.jpeg" /></a>
		        <strong class="chatter_name">Luke Peters</strong>Hello!</span>

		      <span class="feed_box">
		        <a href="" class="chatter_avatar"><img src="https://si0.twimg.com/profile_images/3003485948/349f8da7e56d9abc30f5f405a6ab76c4_bigger.jpeg" /></a>
		        <strong class="chatter_name">Luke Peters</strong>My name is Luke. How can I help you today? :)</span>

		      <span class="chatter_msg_item chatter_msg_item_admin">
		        <a href="" class="chatter_avatar"><img src="https://si0.twimg.com/profile_images/3003485948/349f8da7e56d9abc30f5f405a6ab76c4_bigger.jpeg" /></a>
		        <strong class="chatter_name">Luke Peters</strong>Oh alright, Hello then Jack, you pirate, you.</span>

		   </div>
		   <form method="post" action="#" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo $_POST["id"];?>" class="form">
				<textarea name="chatter_message" placeholder="Type your message here..." class="chatter_field chatter_message"></textarea>
				</textarea>
				<input id="search_button" type="submit" name="submit" value="Submit">
			</form>
		  </div>
		</div>
	</div><br /><br />

<script src="js/skycons.js"></script>
<script>
	var skycons = new Skycons({"color": "#3a3a3a"});

	// Adding a canvis by it's ID if it's found
	if(document.getElementById('clear-day'))
		skycons.add("clear-day", Skycons.CLEAR_DAY);
	if(document.getElementById('clear-night'))
		skycons.add("clear-night", Skycons.CLEAR_NIGHT);
	if(document.getElementById('partly-cloudy-day'))
		skycons.add("partly-cloudy-day", Skycons.PARTLY_CLOUDY_DAY);
	if(document.getElementById('partly-cloudy-night'))
		skycons.add("partly-cloudy-night", Skycons.PARTLY_CLOUDY_NIGHT);
	if(document.getElementById('cloudy'))
		skycons.add("cloudy", Skycons.CLOUDY);
	if(document.getElementById('rain'))
		skycons.add("rain", Skycons.RAIN);
	if(document.getElementById('sleet'))
		skycons.add("sleet", Skycons.SLEET);
	if(document.getElementById('snow'))
		skycons.add("snow", Skycons.SNOW);
	if(document.getElementById('wind'))
		skycons.add("wind", Skycons.WIND);
	if(document.getElementById('fog'))
		skycons.add("fog", Skycons.FOG);

	// Starting animation
	skycons.play();
</script>

<a href="list_trails.php" id="full_button" class="buttons">Back to Trails</a><br /><br />
</div>

</div>


<?php

// Trail Status updates below will work on later

/*
$trailnm = $row["TRAIL_NAME"];

$res = mysqli_query($connection, "select * from `".$trailnm."`");

$row = mysqli_fetch_assoc($res);

$_SESSION["USERNAME"] = $username;
$_SESSION["id"] = $row["id"];


if ($row["STATUS"] == ''){
	} else {
		echo "<h3 class='indt'>Status Updates</h3>";
		echo "<div id='viewtrail_wrapper'>";
		echo ucwords($row["STATUS"]) . "<br />";
		echo ucwords($row["USERNAME"]) . "<br />";
		echo ucwords($row["DATE_ADD"]) . "<br />" . "</div>";
	   }

echo "<br /><br />"

/*
<br /><br />

<div id="viewtrail_wrapper">
	<form method="post" action="view_trail_process.php">
<p class="textform2">Trail Status Update</p> <textarea name="STATUS" class="add_status"><?php echo $_POST["STATUS"];?></textarea>

<input id="submit" type="image" src="images/submit.png" name="save" class="submit_status">
	</form>
</div>
<br />
*/
include("footer.php");
?>