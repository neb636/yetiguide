CREATE TABLE USER (
id int auto_increment not null primary key,
USERNAME     VARCHAR(200) UNIQUE,
PASSWORD	 	 VARCHAR(10),
EMAIL        VARCHAR(40),
FIRST_NAME   VARCHAR(200),
BIRTHDAY	   DATE,
GENDER	     INT NOT NULL,
BIO          VARCHAR(2000),
USER_STATE   VARCHAR(50),
JOINDATE     TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
USER_PICS    VARCHAR(200) NOT NULL,
SKILL        INT)


CREATE TABLE PICTURES (
id int auto_increment not null primary key,
USER_ID_UPLOAD    VARCHAR(200) NOT NULL,
USER_STREAM CHAR(1),
TRAIL_ID_UPLOAD INT,
PICTURE VARCHAR(200))



CREATE TABLE TRAIL (
id int auto_increment not null primary key,
TRAIL_NAME     VARCHAR(200),
DESCRIPTION    VARCHAR(800) NOT NULL,
DIFFICULTY     INT,
RATING         INT,
STATE          CHAR(2) NOT NULL,
LATITUDE	   FLOAT( 10, 6 ) NOT NULL,
LONGITUDE	   FLOAT( 10, 6 ) NOT NULL,
LENGTH         INT,
DURATION       INT,
TRAIL_PICS     VARCHAR(300),
DATE_ADD	   TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
USERNAME       VARCHAR(200)
) ENGINE = MYISAM ;

CREATE TABLE SURVIVAL (
id int auto_increment not null primary key,
GUIDE_NAME     VARCHAR(200),
BODY	       VARCHAR(10000) NOT NULL,
DATE_ADD	   TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
RATING         INT,
USERNAME       VARCHAR(200)
)


insert into survival (GUIDE_NAME, BODY, USERNAME)
values ('Types of Food', 'Great food with lots of food.', 'HENDRIX');


insert into trail (TRAIL_NAME, DESCRIPTION, DIFFICULTY, RATING, STATE, LATITUDE, LENGTH, LONGITUDE, DURATION, TRAIL_PICS, USERNAME)
values ('Arcadia Trail', 'Great hiking trail with lots of great views.', 6, 5, 'RI', '41.5575°N 71.6931°W', 6.7, 4, '', 'H3NDR1X15');

insert into trail (TRAIL_NAME, DESCRIPTION, DIFFICULTY, RAITING, STATE, COORD, LENGTH, DURATION, TRAIL_PICS, USERNAME)
values ('George Washington Management Area', 'Great hiking trail with lots of great views.', 6, 5, 'RI', '41.5575°N 71.6931°W', 6.7, 4, '', 'H3NDR1X15');

insert into trail (TRAIL_NAME, DESCRIPTION, DIFFICULTY, RAITING, STATE, COORD, LENGTH, DURATION, TRAIL_PICS, USERNAME)
values ('Breakheart Pond-Mount Tom Loop', 'Great hiking trail with lots of great views.', 6, 5, 'RI', '41.5575°N 71.6931°W', 6.7, 4, '', 'H3NDR1X15');

insert into trail (TRAIL_NAME, DESCRIPTION, DIFFICULTY, RAITING, STATE, COORD, LENGTH, DURATION, TRAIL_PICS, USERNAME)
values ('East Bay Bike Path', 'Great hiking trail with lots of great views.', 6, 5, 'RI', '41.5575°N 71.6931°W', 6.7, 4, '', 'H3NDR1X15');

insert into trail (TRAIL_NAME, DESCRIPTION, DIFFICULTY, RAITING, STATE, COORD, LENGTH, DURATION, TRAIL_PICS, USERNAME)
values ('Fort Getty Recreation Area', 'Great hiking trail with lots of great views.', 6, 5, 'RI', '41.5575°N 71.6931°W', 6.7, 4, '', 'H3NDR1X15');

insert into trail (TRAIL_NAME, DESCRIPTION, DIFFICULTY, RAITING, STATE, COORD, LENGTH, DURATION, TRAIL_PICS, USERNAME)
values ('Sakonnet Point Ramble', 'Great hiking trail with lots of great views.', 6, 5, 'RI', '41.5575°N 71.6931°W', 6.7, 4, '', 'H3NDR1X15');

insert into trail (TRAIL_NAME, DESCRIPTION, DIFFICULTY, RAITING, STATE, COORD, LENGTH, DURATION, TRAIL_PICS, USERNAME)
values ('Lincoln Wood State Park', 'Great hiking trail with lots of great views.', 6, 5, 'RI', '41.5575°N 71.6931°W', 6.7, 4, '', 'H3NDR1X15');

insert into trail (TRAIL_NAME, DESCRIPTION, DIFFICULTY, RAITING, STATE, COORD, LENGTH, DURATION, TRAIL_PICS, USERNAME)
values ('Big River Management Area', 'Great hiking trail with lots of great views.', 6, 5, 'RI', '41.5575°N 71.6931°W', 6.7, 4, '', 'H3NDR1X15');

insert into trail (TRAIL_NAME, DESCRIPTION, DIFFICULTY, RAITING, STATE, COORD, LENGTH, DURATION, TRAIL_PICS, USERNAME)
values ('Cliff Walk', 'Great hiking trail with lots of great views.', 6, 5, 'RI', '41.5575°N 71.6931°W', 6.7, 4, '', 'H3NDR1X15');

insert into trail (TRAIL_NAME, DESCRIPTION, DIFFICULTY, RAITING, STATE, COORD, LENGTH, DURATION, TRAIL_PICS, USERNAME)
values ('Hurricane Mountain', 'Great hiking trail with lots of great views.', 6, 5, 'NH', '41.5575°N 71.6931°W', 6.7, 4, '', 'H3NDR1X15');

insert into trail (TRAIL_NAME, DESCRIPTION, DIFFICULTY, RAITING, STATE, COORD, LENGTH, DURATION, TRAIL_PICS, USERNAME)
values ('Tuckermans Ravine', 'Great hiking trail with lots of great views.', 6, 5, 'NH', '41.5575°N 71.6931°W', 6.7, 4, '', 'H3NDR1X15');
