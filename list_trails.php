<?php
include("header.php");

$current_page = $_GET["page"];
	if ($current_page < 1) {
		$current_page = 1;
	}
?>

<div class="page_head_block">
	<div class="wrapper">
		<div id="pagename">
			<h1>Trails</h1>
		</div>
	</div>
</div>

<div class="wrapper">

<div id="side" class="side">

	<div id="sidebar_search" class="sidebar">
		<div class="sidebar_top"><h2>Find a trail</h2></div>
		<form  method="post" action="search.php">
		    <input  type="text" name="term" class="form">
		    <input  type="submit" name="submit" value="&#xf002;" id="search" class="buttons">
		</form>
	</div>

	<div id="sidebar_sort" class="sidebar">
		<div class="sidebar_top"><h2>Sort Trails</h2></div>
		<form class="sideform">
			<span class="words">Sort By</span><br /><br />
			<input type="radio" name="sort" value="state"> State<br />
			<input type="radio" name="sort" value="rating"> Rating<br />
			<input type="radio" name="sort" value="date"> Date added
			<br /><br />

			<span class="words">Types of trails</span><br />

			<input type="checkbox" name="type" value="hiking"> Hiking<br />
			<input type="checkbox" name="type" value="biking"> Biking<br />
			<input type="checkbox" name="type" value="ski"> Ski Resourt<br />

			<span class="words">Difficulty</span><br />

			<span class="words">Hours to complete</span><br />

			<span class="words">Rating</span><br />

			<input id="search_button" class="buttons" type="submit" name="submit" value="Search">
	 	</form>
	</div>
</div>

<?php
/* List trails */
$OFFSET = $current_page * 5 - 5;
$res = mysqli_query($connection,"SELECT * FROM trail ORDER BY id LIMIT 5 OFFSET $OFFSET");

if($res) {
	while($row = mysqli_fetch_assoc($res)){
		$id = $row["id"];
		$name = $row["TRAIL_NAME"];
		$position=320; // Defines char I want to display

		// If the title breaks to new line shorten the char so text does not flow out of th box
		if (strlen($name) > 22)
			$position=225;
		$text = $row["DESCRIPTION"];
		$post = substr($text, 0, $position);
		$image = $row["TRAIL_PICS"];

		/* Begin echoing out the content of the loop */
		echo "<a href='view_trail.php?id=$id'><div class='trail_box'><img src='images/$image' class='trailimage'>";
		echo "<h2 class='Trailname'>" . ucwords($name) . "<br /></h2>";

		// Echos stars based on rating value in database
		switch($row["RATING"]) {
			case '1':
				echo "<div id='star'></div><div id='nostar'></div><div id='nostar'></div><div id='nostar'></div><div id='nostar'></div>";
				break;

			case '2':
				echo "<div id='star'></div><div id='star'></div><div id='nostar'></div><div id='nostar'></div><div id='nostar'></div>";
				break;

			case '3':
				echo "<div id='star'></div><div id='star'></div><div id='star'></div><div id='nostar'></div><div id='nostar'></div>";
				break;

			case '4':
				echo "<div id='star'></div><div id='star'></div><div id='star'></div><div id='star'></div><div id='nostar'></div>";
				break;

			case '5':
				echo "<div id='star'></div><div id='star'></div><div id='star'></div><div id='star'></div><div id='star'></div>";
				break;
		}

		echo "<br /><br /><p>State: " . $row["STATE"] . "<br /></p>";
		echo "<p>" . $post . "...." . "</p></div></a>";
	}
}

$res = mysqli_query($connection,"select * from trail");
$pagetype = "trails";
paginate($current_page, $res, $pagetype);
?>

<a href="add_trail.php" id="full_button" class="buttons">Add Trail</a>
</div><br />

<?php
include("footer.php");
?>


