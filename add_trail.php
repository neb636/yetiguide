<?php include("header.php"); ?>
<div id="other"><div class="wrapper"><div id="pagename"><h1>Add Trail</h1></div></div></div>

<div class="wrapper">
<div id="form_wrapper">

	<form method="post" action="trail_process.php" enctype="multipart/form-data">
		<input type="hidden" name="id" value="<?php echo $_POST["id"];?>" class="form">

			<p class="textform">Trail Name</p>
			<input type="text" name="TRAIL_NAME" value="<?php echo $_POST["TRAIL_NAME"];?>" class="form">

			<p class="textform">Trail Type</p>
			<select name="TYPE" class="numrate_form">
				<option value=""></option>
				<option value="Hiking">Hiking</option>
				<option value="Biking">Biking</option>
				<?php echo $_POST["TYPE"]; ?>
			</select>

			<p class="textform">Description</p>
			<textarea name="DESCRIPTION" class="bio">
				<?php echo $_POST["DESCRIPTION"];?>
			</textarea>

			<p class="textform">Difficulty</p>
			<select name="DIFFICULTY" class="numrate_form">
				<option value=""></option>
				<option value="1"> 1</option>
				<option value="2"> 2</option>
				<option value="3"> 3</option>
				<option value="4"> 4</option>
				<option value="5"> 5</option>
				<option value="6"> 6</option>
				<option value="7"> 7</option>
				<option value="8"> 8</option>
				<option value="9"> 9</option>
				<option value="10"> 10</option>
				<?php echo $_POST["DIFFICULTY"];?>
			</select>

			<p class="textform">Rating</p>
			<select name="RATING" class="numrate_form"> &nbsp; 1-5
				<option value=""></option>
				<option value="1"> 1</option>
				<option value="2"> 2</option>
				<option value="3"> 3</option>
				<option value="4"> 4</option>
				<option value="5"> 5</option>
				<?php echo $_POST["RATING"];?>
			</select>

			<p class="textform">State</p>
			<select name="STATE" class="state_form">
				<option value=""></option>
				<option value="AL">Alabama</option>
				<option value="AK">Alaska</option>
				<option value="AZ">Arizona</option>
				<option value="AR">Arkansas</option>
				<option value="CA">California</option>
				<option value="CO">Colorado</option>
				<option value="CT">Connecticut</option>
				<option value="DE">Delaware</option>
				<option value="FL">Florida</option>
				<option value="GA">Georgia</option>
				<option value="HI">Hawaii</option>
				<option value="ID">Idaho</option>
				<option value="IL">Illinois</option>
				<option value="IN">Indiana</option>
				<option value="IA">Iowa</option>
				<option value="KS">Kansas</option>
				<option value="KY">Kentucky</option>
				<option value="LA">Louisiana</option>
				<option value="ME">Maine</option>
				<option value="MD">Maryland</option>
				<option value="MA">Massachusetts</option>
				<option value="MI">Michigan</option>
				<option value="MN">Minnesota</option>
				<option value="MS">Mississippi</option>
				<option value="MO">Missouri</option>
				<option value="MT">Montana</option>
				<option value="NE">Nebraska</option>
				<option value="NV">Nevada</option>
				<option value="NH">New Hampshire</option>
				<option value="NJ">New Jersey</option>
				<option value="NM">New Mexico</option>
				<option value="NY">New York</option>
				<option value="NC">North Carolina</option>
				<option value="ND">North Dakota</option>
				<option value="OH">Ohio</option>
				<option value="OK">Oklahoma</option>
				<option value="OR">Oregon</option>
				<option value="PA">Pennsylvania</option>
				<option value="RI">Rhode Island</option>
				<option value="SC">South Carolina</option>
				<option value="SD">South Dakota</option>
				<option value="TN">Tennessee</option>
				<option value="TX">Texas</option>
				<option value="UT">Utah</option>
				<option value="VT">Vermont</option>
				<option value="VA">Virginia</option>
				<option value="WA">Washington</option>
				<option value="WV">West Virginia</option>
				<option value="WI">Wisconsin</option>
				<option value="WY">Wyoming</option>
				<?php echo $_POST["STATE"]; ?>
			</select>

			<p class="textform">Latitude</p>
			<input type="text" name="LATITUDE" value="<?php echo $_POST["LATITUDE"];?>" class="form">

			<p class="textform">Longitude</p>
			<input type="text" name="LONGITUDE" value="<?php echo $_POST["LONGITUE"];?>" class="form">

			<p class="textform">Length</p>
			<input type="text" name="LENGTH" value="<?php echo $_POST["LENGTH"];?>" class="num_form"> &nbsp; Miles

			<p class="textform">Duration</p>
			<input type="text" name="DURATION" value="<?php echo $_POST["DURATION"];?>" class="num_form"> &nbsp; Hours

			<p class="textform">Elevation Gain</p>
			<input type="text" name="GAIN" value="<?php echo $_POST["GAIN"]; ?>" class="num_form"> &nbsp; Feet

			<p class="textform">Please upload a trail pictures</p>
			<input type="file" name="photo" id="file" size="40" class="margin10"><br>

		<input id="submit" type="image" src="images/submit.png" name="save">
	</form>
</div>
</div>

<?php include("footer.php"); ?>




