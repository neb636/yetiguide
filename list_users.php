<?php
include( 'header.php' );

// Pagination
$current_page = $_GET["page"];
if ( $current_page < 1 ) {
	$current_page = 1;
}

$rows_per_page = 1;
$res = mysqli_query( $connection, "SELECT * FROM user" );
$total_rows = mysqli_num_rows( $res );
$pages = ceil( $total_rows / $rows_per_page );
$OFFSET = $current_page * 15 - 15;

$res = mysqli_query( $connection, "SELECT * FROM user ORDER BY id LIMIT 15 OFFSET $OFFSET" ); ?>

<div class="page_head_block">
	<div class="wrapper">
		<div id="pagename">
			<h1>Users</h1>
		</div>
	</div>
</div>
<div class="wrapper"><center>

	<?php
	// List users
	while ( $row = mysqli_fetch_assoc( $res ) ) {
		$id = $row["id"];
		$user_image = $row["USER_PICS"];

		echo '<a href="view_user.php?id=' . $id .'"><div class="user_box">';

		if ( isset( $user_image ) && !empty( $user_image ) )
			echo '<div class="user_image"><img src="images/' . $user_image . '"></div>';
		else
			echo '<div class="user_image"><img src="images/noimage.jpg"></div>';

		echo '<h4 class="username_echo"><b>' . ucwords( $row["USERNAME"] ) . '</h4></b><p>';

		if ( $row["SKILL"] ==  '0' )
			echo 'Advanced<br/>';
		else
			echo ucwords( $row["SKILL"] ) . '<br/>';

		echo '<span>&#xf041; ' . ucwords( $row["USER_STATE"] ) . '</p></div>';
   }

$res = mysqli_query( $connection, "select * from user" );
$pagetype = "users";
paginate( $current_page, $res, $pagetype );

echo '</div>';

include( 'footer.php' );
