<?php

/**
 * Adds pagination to a page
 * @uses
 * @action
 * @return
 */
function paginate($current_page, $res, $pagetype) {
	$rows_per_page = 5;
	$total_rows = mysqli_num_rows($res);
	$pages = ceil($total_rows / $rows_per_page);
	$next_page = $current_page + 1;
	$previous_page = $current_page - 1;
	$two_back = $current_page - 2;
	$two_forward = $current_page + 2;

	echo "<div id='pagination' class='pagination'>";

	if ($current_page > 1) {
		echo "<a class='pag_button' href='list_$pagetype.php?page=$previous_page'>Previous</a>";
	}
	if ($two_back > 1) {
		echo "<a class='pag' href='list_$pagetype.php?page=$two_back'>$two_back</a>";
	}
	if ($current_page > 1) {
		echo "<a class='pag' href='list_$pagetype.php?page=$previous_page'>$previous_page</a>";
	}
		echo "<a class='pag_button' href='list_$pagetype.php?page=$current_page'>$current_page</a>";
	if ($current_page < $pages) {
		echo "<a class='pag' href='list_$pagetype.php?page=$next_page'>$next_page</a>";
	}
	if ($two_forward < $pages) {
		echo "<a class='pag' href='list_$pagetype.php?page=$two_forward'>$two_forward</a>";
	}
	if ($current_page < $pages) {
		echo " <a class='pag_button' href='list_$pagetype.php?page=$next_page'>Next</a>";
	}

	echo "</div>";
}

/**
 * Accesses forecast.io API
 * @uses
 * @action
 * @return
 */
function forecast($latitude, $longitude) {
	include('inc/class-forecast-io.php');
	$api_key = '67b9bc5d085c6ae8026b6e62d56efaf7';

	$forecast = new ForecastIO($api_key);

	/*
	 * GET CURRENT CONDITIONS
	 */
	$condition = $forecast->getCurrentConditions($latitude, $longitude);
	echo '<canvas id= "'.$condition->getIcon().'" width="100" height="100" class="main_icon"></canvas>';
	echo '<div class="weather_info"><span class="main_temp">' . number_format($condition->getTemperature()) . "&#186;</span>";
	echo '<span class="summary">' . $condition->getSummary() . "</span></div>";
	/*
	 * GET HOURLY CONDITIONS FOR TODAY
	 */
	$conditions_today = $forecast->getForecastToday($latitude, $longitude);

	foreach($conditions_today as $cond) {

	  //echo $cond->getTime('H:i:s') . ': ' . $cond->getTemperature();

	}

	/*
	 * GET DAILY CONDITIONS FOR NEXT 7 DAYS
	 */
	$conditions_week = $forecast->getForecastWeek($latitude, $longitude);

	foreach( $conditions_week as $conditions ) {
		//echo $conditions->getTime('Y.m.d') . ': <canvas id= "'.$condition->getIcon().'" width="80" height="80" class="main_icon"></canvas>';
	}

	/*
	 * GET HISTORICAL CONDITIONS
	 */
	$condition = $forecast->getHistoricalConditions( $latitude, $longitude, strtotime( '2010-10-10T14:00:00-0700' ) );
	// strtotime('2010-10-10T14:00:00-0700') gives timestamp for Pacfic Time... DST shouldn't matter since should be same day

	//echo $condition->getMaxTemperature();
}
?>