<?php
include( 'inc/global.php' );
include( 'functions.php' );

?>
<html>
	<head>
		<title>Yeti Guide Hiking</title>
		<link href="css/screen.css" rel="stylesheet" type="text/css" media="screen"/>
		<meta name="viewport" content="width=device-width">
		<script type="text/javascript" src="//use.typekit.net/qsz1gag.js"></script>
		<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script src="js/jquery.flexslider.js"></script>
		<script>
		function hideAddressBar() {

				if( /iphone|ipod|android/gi.test( navigator.userAgent ) && !/crios/gi.test( navigator.userAgent ) ) {
					// Events that should trigger the address bar to hide
					window.addEventListener( 'load', removeAddressBar, false );
					window.addEventListener( 'orientationchange', removeAddressBar, false );
				}

			}
				</script>
				<script>
		if( window.orientation === 0 ) {
					document.documentElement.style.overflow = 'scroll';
					document.body.style.height = '120%';
				}
				else {
					document.documentElement.style.overflow = '';
					document.body.style.height = '100%';
				}

				setTimeout( function() {
					window.scrollTo( 0, 1 );
				}, 10 );
		</script>

		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
		<meta name="robots" content="noindex">
	</head>

   <body onload="hideAddressBar();">
	<div class="top">
		<div class="wrapper">
			<?php
				//($row["USERNAME"]) --- ADD THIS TO DO INSIDE
				if ($_SESSION["USERNAME"] != '') {
					echo "<a href='view_login.php' id='logins' class='underline'>" . $_SESSION['USERNAME'] . "</a>&nbsp; &nbsp; &nbsp;";
					echo "<a href='logout.php' id='logins'>Log Out</a>";
				} else {
					echo "&nbsp; &nbsp; <a href='login.php' id='logins'>Login</a> &nbsp; &nbsp; <a href='signup.php' id='logins'>  Sign Up</a>";
				}
			?>
		</div>
	</div>

	<div id="header">
		<div class="wrapper">
			<?php
				//($row["USERNAME"]) --- ADD THIS TO DO INSIDE
				if ($_SESSION["USERNAME"] != '') {
					echo "<a href='view_login.php' id='logins' class='underline'>" . $_SESSION['USERNAME'] . "</a>&nbsp; &nbsp; &nbsp;";
				}
				else {
					echo "&nbsp; &nbsp; <a href='login.php' id='logins'>Login</a>";
				}
			?>

			<div id="logo"><a href="index.php"><img src="images/logo2.png" alt="Yeti Guide logo and link to homepage" width="82" height="49" id="logo2" class="dim"></a></div>

				<nav>
					<ul>
						<li><a href="about.php">ABOUT</a>
							<ul>
								<li><a href="">Our Philosophy</a></li>
								<li><a href="">The App</a></li>
							</ul>
						<li><a href="list_trails.php">TRAILS</a></li>
						<li><a href="list_users.php">USERS</a></li>
						<li><a href="">FORUMS</a></li>
					</ul>
				</nav>

				<button class="menu_tab">
						<ul>
							<li><a href="about.php">ABOUT</a>
								<ul>
									<li><a href="">Our Philosophy</a></li>
									<li><a href="">The App</a></li>
								</ul>
							<li><a href="list_trails.php">TRAILS</a></li>
							<li><a href="list_users.php">USERS</a></li>
							<li><a href="">FORUMS</a></li>
						</ul>
				</button>


		</div>
	</div>




