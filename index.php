<?php include( 'header.php' ); ?>

	<div id="slider" class="slider">
		<div class="wrapper">
			<p class="headline">Don't get lost <br /> Get Yeti Guide</p>
			<img class="app" src="images/phone3.png">
			<img class="apple dim2" src="images/app.png">
		</div>
	</div>

		<div id="tagline_home" class="tagline_home">
			<div class="wrapper">
				<h1 class="tagline">YOUR TRUSTED GUIDE TO THE OUTDOORS</h1>
			</div>
		</div>

		<div class="home_body">
			<div class="wrapper">

				<div class="icon_wrapper">
					<img src="images/location.png" class="location">
					<center><h4 class="icon_text">LOCATION</h4></center>
					<p class="icon_about">
					Yeti Guide allows you to always know where you are. It also saves your last location in a database every 10 minutes incase of an emergency.	This can give your loved ones piece of mind.</p>
				</div>

				<div class="icon_wrapper">
					<img src="images/map.png" class="maps">
					<center><h4 class="icon_text">USER PLOTTED TRAILS</h4></center>
					<p class="icon_about">
					Trails are added by seniors members in a wiki fashion using the mobile app. Just turn on map new trail and start your hike.
					It will be added to the database when you are done.</p>
				</div>

				<div class="icon_wrapper">
					<img src="images/cloud.png" class="icon">
					<center><h4 class="icon_text">WEATHER</h4></center>
					<p class="icon_about">
					Yeti Guide tells you all the weather for your trip. If there are any serviere weather warnings Yeti Guide will automatically tell you during your hike.</p>
				</div>

				<div class="icon_wrapper">
					<img src="images/chat.png" class="chat">
					<center><h4 class="icon_text">MESSAGING</h4></center>
					<p class="icon_about">
					Yeti Guide allows you to talk to other users on your trail. A marker can indicates where you are and once clicked allows you to chat.
					This feature can be tured off for privacy and saftey reasons.</p>
				</div>

			</div>
		</div>

		<div id="saying">
			<div class="wrapper">

				<div class="ribbon_wrapper"><img src="images/ribbon.png" class="ribbon"></div>

				<div class="people">
					<img src="images/heather.png" class="circle">
					<p class="quote">'Yeti Guide Rocks! I use it every time I go hiking. I love that it records my location every 10 minutes incase something happens.'</p> <br />
					<p class="names"><b>- Heather Williams</b></p>
				</div>

				<div class="people">
					<img src="images/jason2.png" class="circle">
					<p class="quote">'I love that Yeti Guide allows me to map new trails and share them with my fellow hikers. This is a feature that I think is amazing.'</p> <br />
					<p class="names"><b>- Jason Cote</b></p>
				</div>

				<div class="people">
					<img src="images/george.png" class="circle">
					<p class="quote">'The messaging feature of Yeti Guide is amazing! It makes it easier to make new friends on the trail.'</p> <br />
					<p class="george"><b>- George Augar</b></p>
				</div>
		</div>
	</div>
</div>

</div>

<?php include("footer.php"); ?>