//AIzaSyBssqQ0yD7wUOkrLbQ7e2emhPuCzL4elow
var lat = <?php echo $latitude; ?>;
var lon = <?php echo $longitude; ?>;
var map;
function initialize() {
  var mapOptions = {
    zoom: 8,
    center: new google.maps.LatLng(lat, lon),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);
}

google.maps.event.addDomListener(window, 'load', initialize);