Yeti Guide
==========

![main-screen 1](https://user-images.githubusercontent.com/4202152/28334203-6a4fa90c-6bc8-11e7-898b-dac7262b232e.png =100x)

First ever coding project from back in 2013. Social hiking trail guide/listing site.

---
![homepage](https://user-images.githubusercontent.com/4202152/28334214-717dccd6-6bc8-11e7-8a52-f354ae7fb753.png =500x)

![main-screen 1](https://user-images.githubusercontent.com/4202152/28334207-6bafd25e-6bc8-11e7-88a2-edb680c823f4.png =500x)

![1](https://user-images.githubusercontent.com/4202152/28334219-741a61a2-6bc8-11e7-9de4-3a6800f727d8.png =500x)

![menus](https://user-images.githubusercontent.com/4202152/28334230-7b1af6e2-6bc8-11e7-87c6-9450f6f6ba83.png =500x)