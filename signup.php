<?php
include( 'header.php' ); ?>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js"></script>

<div class="page_head_block">
	<div class="wrapper">
		<div id="pagename">
			<h1>Sign Up</h1>
		</div>
	</div>
</div>

<div class="wrapper">
	<div id="subhead" class="subhead">
		<h2 class="sub_headline">It's time to join a great community</h2>
		<h3 class="under_headline">We are very excited to have you on board. Make sure to introduce yourself to the community in the forms right away</h3>
	</div>

	<div id="form_left" class="form_left">
	<span>1. Username and password</span>
	</div>
	<div id="form_right" class="form_right">
		<form id="signup_form" method="post" action="process_pages/signup_process.php" enctype="multipart/form-data">
			<input type="hidden" name="id" value="<?php echo $_POST['id']; ?>">

			<p class="textform">Username</p>
			<input id="cname" type="text" name="USERNAME" value="<?php echo $_POST['USERNAME'];?>" class="signup_field" minlength="2" maxlength="17" required/>
			<p class="break">Can be no more than 17 characters long</p>

			<div class="float_left break">
			<p class="textform">Password</p>
			<input type="password" name="PASSWORD" value="<?php echo $_POST['PASSWORD']; ?>" class="signup_field">
			</div>
			<div class="stack_left">
				<p class="textform">Reenter Password</p>
				<input type="password" name="PASSWORD2" value="<?php echo $_POST['PASSWORD2"']; ?>" class="signup_field">
			</div>
			<p class="break">Must be between 6 and 10 characters long</p>

			<p class="textform">Email</p>
			<input id="cemail" type="email" name="EMAIL" value="<?php echo $_POST['EMAIL']; ?>" class="signup_field" required/><br />

			<p class="textform">Name</p>
			<input type="textarea" name="FIRST_NAME" value="<?php echo $_POST['FIRST_NAME']; ?>" class="signup_field">
			<p class="break">Optional</p>

			<p class="textform">Gender</p>
			<select name="GENDER" class="gender_form">
				<option value=""></option>
				<option value="Male">Male</option>
				<option value="Female" >Female</option> <?php echo $_POST['GENDER']; ?>
			</select>

			<p class="textform">Birthday</p>
			<select name="BIRTH_MONTH" class="state_form">
        <option value="1">January</option>
        <option value="2">February</option>
        <option value="3">March</option>
        <option value="4">April</option>
        <option value="5">May</option>
        <option value="6">June</option>
        <option value="7">July</option>
        <option value="8">August</option>
        <option value="9">September</option>
        <option value="10">October</option>
        <option value="11">November</option>
        <option value="12">December</option>
      </select>
      <select name="BIRTH_DAY" class="state_form">
	      <?php
	      for ($i=1; $i<=31; $i++)
	        echo "<option value='$i'>$i</option>"; ?>
      </select>
      <select name="BIRTH_YEAR" class="state_form">
        <?php
        for ( $i = date('Y'); $i>=1910; $i=$i-1 )
          echo "<option value='$i'>$i</option>"; ?>
      </select>

			<p class="textform">State</p>
			<select name="USER_STATE" class="state_form">
				<option value=""></option>
				<option value="AL">Alabama</option>
				<option value="AK">Alaska</option>
				<option value="AZ">Arizona</option>
				<option value="AR">Arkansas</option>
				<option value="CA">California</option>
				<option value="CO">Colorado</option>
				<option value="CT">Connecticut</option>
				<option value="DE">Delaware</option>
				<option value="FL">Florida</option>
				<option value="GA">Georgia</option>
				<option value="HI">Hawaii</option>
				<option value="ID">Idaho</option>
				<option value="IL">Illinois</option>
				<option value="IN">Indiana</option>
				<option value="IA">Iowa</option>
				<option value="KS">Kansas</option>
				<option value="KY">Kentucky</option>
				<option value="LA">Louisiana</option>
				<option value="ME">Maine</option>
				<option value="MD">Maryland</option>
				<option value="MA">Massachusetts</option>
				<option value="MI">Michigan</option>
				<option value="MN">Minnesota</option>
				<option value="MS">Mississippi</option>
				<option value="MO">Missouri</option>
				<option value="MT">Montana</option>
				<option value="NE">Nebraska</option>
				<option value="NV">Nevada</option>
				<option value="NH">New Hampshire</option>
				<option value="NJ">New Jersey</option>
				<option value="NM">New Mexico</option>
				<option value="NY">New York</option>
				<option value="NC">North Carolina</option>
				<option value="ND">North Dakota</option>
				<option value="OH">Ohio</option>
				<option value="OK">Oklahoma</option>
				<option value="OR">Oregon</option>
				<option value="PA">Pennsylvania</option>
				<option value="RI">Rhode Island</option>
				<option value="SC">South Carolina</option>
				<option value="SD">South Dakota</option>
				<option value="TN">Tennessee</option>
				<option value="TX">Texas</option>
				<option value="UT">Utah</option>
				<option value="VT">Vermont</option>
				<option value="VA">Virginia</option>
				<option value="WA">Washington</option>
				<option value="WV">West Virginia</option>
				<option value="WI">Wisconsin</option>
				<option value="WY">Wyoming</option>
				<?php echo $_POST['USER_STATE']; ?>
			</select>

			<p class="textform">Skill</p>
			<select name="SKILL" class="skill_form">
				<option value=""></option>
				<option value="Beginner">Beginner</option>
				<option value="Intermediate" >Intermediate</option>
				<option value="Advanced">Advanced</option>  <?php echo $_POST['SKILL']; ?>
			</select>

			<p class="textform">Biography</p>
			<textarea name="BIO" class="bio"><?php echo $_POST['BIO']; ?></textarea>
			<p class="break">Optional</p>

			<p class="textform">Please upload a profile picture</p>
			<input type="file" name="photo" id="file" size="40" class="margin10">
			<br><hr>

			<div id="captcha">
				<img src="inc/captcha_image.php">
		    </div>

		    <p class="enter"><b>Enter text below</b></span><br />
			<input type="text" name="guess" class="form"><br /><br />

			<input type="submit" id="full_button" class="buttons" value="Submit" name="save">

		</form>
		<script> $("#signup_form").validate(); </script>
	</div>
</div></div>

<?php
include( 'footer.php' );