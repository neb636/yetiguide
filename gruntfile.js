module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      options: {
        separator: ';'
      },
      dist: {
        src: ['js/intro.js', 'js/project.js', 'js/outro.js'],
        dest: 'js/src/built.js'
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: 'js/src/<%= pkg.name %>.js',
        dest: 'js/src/build/<%= pkg.name %>.min.js'
      }
    },
    imagemin: {                          // Task
      dist: {                            // Target
        options: {                       // Target options
          optimizationLevel: 7
        },
        dynamic: {                         // Another target
          files: [{
            expand: true,                  // Enable dynamic expansion
            //cwd: '',                   // Src matches are relative to this path
            src: ['images/**/*.{png,jpg,gif}'],   // Actual patterns to match
            dest: 'images/src/'                  // Destination path prefix
          }]
        }
      }
    },
    compass: {
      dist: {
        options: {
          config: 'config.rb'
        }
      }
    },
    csscss: {
      options: {
        require: 'config.rb'
      },
      dist: {
        src: ['sass/screen.scss']
      }
    },
    git_ftp: {
      production: {
        options: {
          'hostFile':'.gitftppass',
          'host':'default'
        }
      }
    }
  });

  // Load the plugins
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-csscss');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-git-ftp');
  grunt.loadNpmTasks('grunt-contrib-imagemin');


  // Default task(s).
  grunt.registerTask('default', ['compass', 'imagemin', 'ftp']);
};