<?php
include( 'header.php' );

// Pagination
$current_page = $_GET['page'];
if ($current_page < 1) {
	$current_page = 1;
}

$rows_per_page = 10;
$res = mysqli_query( $connection, "SELECT * FROM survival" );
$total_rows = mysqli_num_rows( $res );
$pages = ceil( $total_rows / $rows_per_page );
$OFFSET = $current_page * 10 - 10;
?>


<div id="list_trail_body">

<center><h1>Survival Guides</h1></center><br><br>


<?php
echo '<br /><br />';

/* List guides */

$res = mysqli_query( $connection, "SELECT * FROM SURVIVAL ORDER BY id LIMIT 10 OFFSET $OFFSET" );

$name = $row['GUIDE_NAME'];

	while( $row = mysqli_fetch_assoc( $res ) ) {
		$id = $row['id'];
		$position = 120; // Defines char I want to display
		$text = $row['BODY'];
		$post = substr( $text, 0, $position );

		echo '<div id="traillist"><span class="Trailname">';
		echo ucwords( $row['GUIDE_NAME'] ) . '<br />';
		echo '</span>';

		echo $post . '....' . '<br />';

// Pagination
$next_page = $current_page + 1;
$previous_page = $current_page - 1;

if ( $current_page > 1 ) {
	echo "<a href='survival_list.php?page=$previous_page'>Back</a>";
}
echo " current page " . $current_page . " of " . $pages . " ";

if ($current_page < $pages) {
	echo " <a href='survival_list.php?page=$next_page'>Next</a>";
}
?>

<h2><a href="add_guide.php">Add a survival guide</a></h2>
</div>

<?php
include("footer.php");
?>